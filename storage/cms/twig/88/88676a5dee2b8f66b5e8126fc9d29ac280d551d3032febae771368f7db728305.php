<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/install-master/themes/responsiv-flat/pages/portfolio/project.htm */
class __TwigTemplate_61dd35d6c57bf98b1d05500c45be9bedf2341be2a2504cef111e86a4892b69ca extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('styles'        );
        // line 2
        echo "    <link href=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter([0 => "assets/vendor/slick/slick.css"]);
        // line 4
        echo "\" rel=\"stylesheet\">
";
        // line 1
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->startBlock('scripts'        );
        // line 7
        echo "    <script src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter([0 => "assets/vendor/slick/slick.js"]);
        // line 9
        echo "\"></script>
    <script>
        \$(document).ready(function() {
            \$('#projectImages').slick({
                dots: true,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                slidesToShow: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                pauseOnHover: true
            });
        })
    </script>
";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->endBlock(true        );
        // line 26
        echo "
<section id=\"layout-title\">
    <div class=\"container\">
        <h3>Wedding anniversary</h3>
    </div>
</section>

<div class=\"container\">

    <div id=\"projectImages\" class=\"project-images\">
        <div>
            <div class=\"image\"><img src=\"";
        // line 37
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/portfolio/project1.jpg");
        echo "\" class=\"img-responsive\" alt=\"\" /></div>
            <div class=\"caption\">Classic vintage</div>
        </div>
        <div>
            <div class=\"image\"><img src=\"";
        // line 41
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/portfolio/project2.jpg");
        echo "\" class=\"img-responsive\" alt=\"\" /></div>
            <div class=\"caption\">Nature's prime</div>
        </div>
        <div>
            <div class=\"image\"><img src=\"";
        // line 45
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/portfolio/project3.jpg");
        echo "\" class=\"img-responsive\" alt=\"\" /></div>
            <div class=\"caption\">Surfin' bird</div>
        </div>
        <div>
            <div class=\"image\"><img src=\"";
        // line 49
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/portfolio/project4.jpg");
        echo "\" class=\"img-responsive\" alt=\"\" /></div>
            <div class=\"caption\">Healthy, wealthy &amp; wise</div>
        </div>
    </div>

    <div class=\"row\">

        <div class=\"col-md-9\">

            <!-- Project description -->
            <div class=\"project-description\">
                <p>There's a voice that keeps on calling me. Down the road, that's where I'll always be. Every stop I make, I make a new friend. Can't stay for long, just turn around and I'm gone again. Maybe tomorrow, I'll want to settle down, Until tomorrow, I'll just keep moving on.</p>
                <p>Bacon ipsum dolor sit amet prosciutto kielbasa meatball short loin. Jowl shankle doner pancetta, turkey short loin capicola bresaola prosciutto beef ribs cow. Tenderloin salami pork belly capicola.</p>
                <p>T-bone spare ribs fatback pork andouille, meatloaf chicken beef beef ribs. Salami beef ribs pig ground round chicken, tail doner tenderloin shankle prosciutto biltong chuck. Drumstick swine hamburger tongue strip steak jowl jerky sausage. Strip steak pastrami drumstick, pork chop meatloaf pig turkey doner beef ribs ground round turducken.</p>
            </div>

        </div>
        <div class=\"col-md-3\">

            <!-- Project information -->
            <ul class=\"project-info\">
                <li><strong>Client</strong> Wdding anniversary</li>
                <li><strong>Date</strong> August 2019</li>
            </ul>
            <a href=\"\" class=\"btn btn-info btn-lg btn-block\">Project page</a>

        </div>
    </div>

    <!-- Quote -->
    <h4 class=\"headline\"><span>What the client said</span></h4>
    <blockquote>
        <p>These guys are very cooperative, friendly andrather helpful. But most importantly hardworking</p>
        <p> It was my first time with them, I wanted our surprise wedding anniversary to be special for my wife.</p>
        <p> They delivered as promised and swept everyone off their feet, my wife was moved to tears! I would highly recommend them</p>
        <footer>A very satisfied cient <cite title=\"Source Title\"> Nairobi</cite></footer>
    </blockquote>

  
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/install-master/themes/responsiv-flat/pages/portfolio/project.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 49,  97 => 45,  90 => 41,  83 => 37,  70 => 26,  68 => 6,  50 => 9,  47 => 7,  45 => 6,  43 => 1,  40 => 4,  37 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% put styles %}
    <link href=\"{{ [
        'assets/vendor/slick/slick.css',
    ]|theme }}\" rel=\"stylesheet\">
{% endput %}
{% put scripts %}
    <script src=\"{{ [
        'assets/vendor/slick/slick.js',
    ]|theme }}\"></script>
    <script>
        \$(document).ready(function() {
            \$('#projectImages').slick({
                dots: true,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                slidesToShow: 1,
                autoplay: true,
                autoplaySpeed: 2000,
                pauseOnHover: true
            });
        })
    </script>
{% endput %}

<section id=\"layout-title\">
    <div class=\"container\">
        <h3>Wedding anniversary</h3>
    </div>
</section>

<div class=\"container\">

    <div id=\"projectImages\" class=\"project-images\">
        <div>
            <div class=\"image\"><img src=\"{{ 'assets/images/portfolio/project1.jpg'|theme }}\" class=\"img-responsive\" alt=\"\" /></div>
            <div class=\"caption\">Classic vintage</div>
        </div>
        <div>
            <div class=\"image\"><img src=\"{{ 'assets/images/portfolio/project2.jpg'|theme }}\" class=\"img-responsive\" alt=\"\" /></div>
            <div class=\"caption\">Nature's prime</div>
        </div>
        <div>
            <div class=\"image\"><img src=\"{{ 'assets/images/portfolio/project3.jpg'|theme }}\" class=\"img-responsive\" alt=\"\" /></div>
            <div class=\"caption\">Surfin' bird</div>
        </div>
        <div>
            <div class=\"image\"><img src=\"{{ 'assets/images/portfolio/project4.jpg'|theme }}\" class=\"img-responsive\" alt=\"\" /></div>
            <div class=\"caption\">Healthy, wealthy &amp; wise</div>
        </div>
    </div>

    <div class=\"row\">

        <div class=\"col-md-9\">

            <!-- Project description -->
            <div class=\"project-description\">
                <p>There's a voice that keeps on calling me. Down the road, that's where I'll always be. Every stop I make, I make a new friend. Can't stay for long, just turn around and I'm gone again. Maybe tomorrow, I'll want to settle down, Until tomorrow, I'll just keep moving on.</p>
                <p>Bacon ipsum dolor sit amet prosciutto kielbasa meatball short loin. Jowl shankle doner pancetta, turkey short loin capicola bresaola prosciutto beef ribs cow. Tenderloin salami pork belly capicola.</p>
                <p>T-bone spare ribs fatback pork andouille, meatloaf chicken beef beef ribs. Salami beef ribs pig ground round chicken, tail doner tenderloin shankle prosciutto biltong chuck. Drumstick swine hamburger tongue strip steak jowl jerky sausage. Strip steak pastrami drumstick, pork chop meatloaf pig turkey doner beef ribs ground round turducken.</p>
            </div>

        </div>
        <div class=\"col-md-3\">

            <!-- Project information -->
            <ul class=\"project-info\">
                <li><strong>Client</strong> Wdding anniversary</li>
                <li><strong>Date</strong> August 2019</li>
            </ul>
            <a href=\"\" class=\"btn btn-info btn-lg btn-block\">Project page</a>

        </div>
    </div>

    <!-- Quote -->
    <h4 class=\"headline\"><span>What the client said</span></h4>
    <blockquote>
        <p>These guys are very cooperative, friendly andrather helpful. But most importantly hardworking</p>
        <p> It was my first time with them, I wanted our surprise wedding anniversary to be special for my wife.</p>
        <p> They delivered as promised and swept everyone off their feet, my wife was moved to tears! I would highly recommend them</p>
        <footer>A very satisfied cient <cite title=\"Source Title\"> Nairobi</cite></footer>
    </blockquote>

  
</div>", "/var/www/html/install-master/themes/responsiv-flat/pages/portfolio/project.htm", "");
    }
}
