<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/install-master/themes/responsiv-flat/pages/samples/contact.htm */
class __TwigTemplate_565d8e7389c9c142b385e140de27c30430a5de0673e696f1305c7a4c8dfb37cd extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<section id=\"layout-title\">
    <div class=\"container\">
        <h3>Contact us</h3>
    </div>
</section>

<div class=\"container\">
    <div class=\"row\">

        <div class=\"col-md-7\">
            <p>Interested? Reach out. </p>
            <form role=\"form\">
                <div class=\"form-group\">
                    <label for=\"email\">Your email address</label>
                    <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Enter email\">
                </div>
                <div class=\"form-group\">
                    <label for=\"name\">Your name</label>
                    <input type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"Enter name\">
                </div>
                <div class=\"form-group\">
                    <label for=\"message\">Your message</label>
                    <textarea class=\"form-control\" rows=\"7\" id=\"message\" placeholder=\"Enter message\" style=\"resize: none\"></textarea>
                </div>

                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <button type=\"submit\" class=\"btn btn-lg btn-info\">Send message</button>
                    </div>
                    <div class=\"col-sm-6\">
                        <label class=\"checkbox pull-right\" for=\"checkbox1\">
                            <input type=\"checkbox\" value=\"\" id=\"checkbox1\" data-toggle=\"checkbox\">
                            Send a copy to myself
                        </label>
                    </div>
                </div>
            </form>
            <br /><br />

        </div>

        <div class=\"col-md-5\">
            <div class=\"contact-banner\">
                <h3 class=\"banner-title\">Our location</h3>
                <ul>
                  
                    <li>Nairobi Kenya, </li>
                    <li>Phone: +254 740 597 993 / 
                            +254 784 781 319 </li>
                    
                    <li>Email: <a href=\"#\">seasonedoccasion@gmail.com</a></li>
                </ul>
            </div>

            <div class=\"google-maps\">
              <iframe
                width=\"500\"
                height=\"450\"
                frameborder=\"0\"
                scrolling=\"no\"
                marginheight=\"0\"
                marginwidth=\"0\"
                src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d255282.32375088028!2d36.70696377216464!3d-1.303205036839981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1172d84d49a7%3A0xf7cf0254b297924c!2sNairobi%2C+Kenya!5e0!3m2!1sen!2sbg!4v1553868303620!5m2!1sen!2sbg\"></iframe>
            </div>

        </div>

    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/install-master/themes/responsiv-flat/pages/samples/contact.htm";
    }

    public function getDebugInfo()
    {
        return array (  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<section id=\"layout-title\">
    <div class=\"container\">
        <h3>Contact us</h3>
    </div>
</section>

<div class=\"container\">
    <div class=\"row\">

        <div class=\"col-md-7\">
            <p>Interested? Reach out. </p>
            <form role=\"form\">
                <div class=\"form-group\">
                    <label for=\"email\">Your email address</label>
                    <input type=\"email\" class=\"form-control\" id=\"email\" placeholder=\"Enter email\">
                </div>
                <div class=\"form-group\">
                    <label for=\"name\">Your name</label>
                    <input type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"Enter name\">
                </div>
                <div class=\"form-group\">
                    <label for=\"message\">Your message</label>
                    <textarea class=\"form-control\" rows=\"7\" id=\"message\" placeholder=\"Enter message\" style=\"resize: none\"></textarea>
                </div>

                <div class=\"row\">
                    <div class=\"col-sm-6\">
                        <button type=\"submit\" class=\"btn btn-lg btn-info\">Send message</button>
                    </div>
                    <div class=\"col-sm-6\">
                        <label class=\"checkbox pull-right\" for=\"checkbox1\">
                            <input type=\"checkbox\" value=\"\" id=\"checkbox1\" data-toggle=\"checkbox\">
                            Send a copy to myself
                        </label>
                    </div>
                </div>
            </form>
            <br /><br />

        </div>

        <div class=\"col-md-5\">
            <div class=\"contact-banner\">
                <h3 class=\"banner-title\">Our location</h3>
                <ul>
                  
                    <li>Nairobi Kenya, </li>
                    <li>Phone: +254 740 597 993 / 
                            +254 784 781 319 </li>
                    
                    <li>Email: <a href=\"#\">seasonedoccasion@gmail.com</a></li>
                </ul>
            </div>

            <div class=\"google-maps\">
              <iframe
                width=\"500\"
                height=\"450\"
                frameborder=\"0\"
                scrolling=\"no\"
                marginheight=\"0\"
                marginwidth=\"0\"
                src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d255282.32375088028!2d36.70696377216464!3d-1.303205036839981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1172d84d49a7%3A0xf7cf0254b297924c!2sNairobi%2C+Kenya!5e0!3m2!1sen!2sbg!4v1553868303620!5m2!1sen!2sbg\"></iframe>
            </div>

        </div>

    </div>
</div>", "/var/www/html/install-master/themes/responsiv-flat/pages/samples/contact.htm", "");
    }
}
