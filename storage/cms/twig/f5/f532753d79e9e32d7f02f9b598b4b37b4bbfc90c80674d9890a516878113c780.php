<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /var/www/html/install-master/themes/responsiv-flat/partials/services.htm */
class __TwigTemplate_26235d6e0036c4438091fd486f22bdaaec6c1c5f12370e5db77f2d19d0c15510 extends \Twig\Template
{
    private $source;

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<div class=\"row\">
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-gear\"></i>
                <div class=\"service-desc\">
                    <h5> Tents design Package</h5>
                    <p>We offer unique tent's packaging of the latest emerging trends that will set each customer event different from the rest</p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-home\"></i>
                <div class=\"service-desc\">
                    <h5> Tent Setup</h5>
                    <p>Besides offering tent designs, we setup the event for you at your desired location. </p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-refresh\"></i>
                <div class=\"service-desc\">
                    <h5>Easy to customize</h5>
                    <p>Need a last minute change? You can do so 2 days before the event date.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-plus\"></i>
                <div class=\"service-desc\">
                    <h5>Additional services</h5>
                    <p>Accessories such as linen, interior lighting, tents and chairs are offered as a complement to our clients.</p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-envelope\"></i>
                <div class=\"service-desc\">
                    <h5>24/7 support</h5>
                    <p>During your event, a team will be on the grounds for any support needed.</p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-image\"></i>
                <div class=\"service-desc\">
                    <h5>Special Portfolio</h5>
                    <p>Photography services are an added bonus!!</p>
                </div>
            </div>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/html/install-master/themes/responsiv-flat/partials/services.htm";
    }

    public function getDebugInfo()
    {
        return array (  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"row\">
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-gear\"></i>
                <div class=\"service-desc\">
                    <h5> Tents design Package</h5>
                    <p>We offer unique tent's packaging of the latest emerging trends that will set each customer event different from the rest</p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-home\"></i>
                <div class=\"service-desc\">
                    <h5> Tent Setup</h5>
                    <p>Besides offering tent designs, we setup the event for you at your desired location. </p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-refresh\"></i>
                <div class=\"service-desc\">
                    <h5>Easy to customize</h5>
                    <p>Need a last minute change? You can do so 2 days before the event date.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-plus\"></i>
                <div class=\"service-desc\">
                    <h5>Additional services</h5>
                    <p>Accessories such as linen, interior lighting, tents and chairs are offered as a complement to our clients.</p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-envelope\"></i>
                <div class=\"service-desc\">
                    <h5>24/7 support</h5>
                    <p>During your event, a team will be on the grounds for any support needed.</p>
                </div>
            </div>
        </div>
    </div>
    <div class=\"col-sm-4\">
        <div class=\"services\">
            <div class=\"service-item\">
                <i class=\"fa fa-image\"></i>
                <div class=\"service-desc\">
                    <h5>Special Portfolio</h5>
                    <p>Photography services are an added bonus!!</p>
                </div>
            </div>
        </div>
    </div>
</div>", "/var/www/html/install-master/themes/responsiv-flat/partials/services.htm", "");
    }
}
